@extends('layouts.master')

@section('content')
<div class="row" style="margin-top:40px">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            <div class="card-header text-center">
                Modificar cliente
            </div>
            <div class="card-body" style="padding:30px">

                <form action="/foo/bar" method="POST">
                    @method('PUT')
                    <!-- <input type="hidden" name="_method" value="PUT"> -->
                </form>

                {{-- TODO: Protección contra CSRF --}}

                <div class="form-group">
                    <label for="title">Nombre</label>
                    <input type="text" name="title" id="title" class="form-control">
                </div>

                <div class="form-group">
                    {{-- TODO: Completa el input para la imagen --}}
                </div>

                <div class="form-group">
                    {{-- TODO: Completa el input para el fecha de nacimiento --}}
                </div>

                <div class="form-group">
                    {{-- TODO: Completa el input para el correo --}}
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                        Modificar cliente
                    </button>
                </div>

                {{-- TODO: Cerrar formulario --}}

            </div>
        </div>
    </div>
</div>
@stop