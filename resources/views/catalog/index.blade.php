@extends('layouts.master')

@section('content')
    <h1>Catálogo</h1>
    <div class="row">
        @foreach( $cliente as $key => $client )

            <div class="col-xs-6 col-sm-4 col-md-3 text-center">
                <a href="{{ url('/catalog/show/' . $client->id ) }}">
                    <img src="{{$client->imagen}}" style="height:200px" />
                    <h4 style="min-height:45px;margin:5px 0 10px 0">
                        {{$client->name}}
                    </h4>
                </a>
            </div>
        @endforeach
        {{$cliente->links()}}
    </div>
@stop 