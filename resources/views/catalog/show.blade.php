@extends('layouts.master')

@section('content')
    <h1>Show</h1>
    
    <div class="row">

        <div class="col-sm-4">

            <img width="400px" src="{{$cliente->imagen}}">

        </div>

        <div class="col-sm-8 p-2">
            <h3>
                {{$cliente->name}}
            </h3>
            <p>
                Correo: {{$cliente->correo}}
            </p>
            <p>
                Fecha de Nacimiento: {{$cliente->fecha_nacimiento}}
            </p>
            <a class="btn btn-danger"> Editar </a>
            <a class="btn btn-warning"> Retroceder </a>
            <a class="btn btn-danger"> España </a>
        </div>
    </div>
@stop 