<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PrubeController;
use App\Http\Controllers\treintayseis;

//Ejercicios Anteriores
//{
    //Apartado 2 ---A32
    //{
        // Route::get('/', function () {
        //     return view('home');
        // });

        // Route::get('auth/login', function () {
        //     return view('auth/login');
        // });

        // Route::get('auth/logout', function () {
        //     return ('Logout usuario');
        // });

        // Route::get('catalog', function () {
        //     return view('catalog/index');
        // });

        // Route::get('catalog/show/{id}', function ($id) {
        //     return view('catalog/show', compact('id'));
        // });

        // Route::get('catalog/create', function () {
        //     return view('catalog/create');
        // });

        // Route::get('catalog/edit/{id}', function ($id) {
        //     return view('catalog/edit', compact('id'));
        // });

        // Route::get('catalog/delete/{id}', function ($id) {
        //     return view('catalog/delete', compact('id'));
        // });


        // Route::get('host',function(){
        //     return env('DB_HOST');
        // });
        
        // Route::get('timezone',function(){
        //     return config('app.timezone');
        // });
        
    //}

    // Apartado 3 ---A32
    //{
        //  Route::get('/ej12/{name?}', function ($name = 'Luis'){
        //     return $name;
        //  });

        //  Route::post('/ej3', function ($ej3 = 'Difunto'){return $ej3; });

        //  Route::any('/ej4',function($ej4 = "Estoy manolo"){
        //      return $ej4;
        // });
        //     Route::get('/ej5/{id}', function ($id) {
        //         return $id;
        //     })->where('id', '[0-9]+');
        //     Route::get('/ej6/{letr}/{id}', function ($letr,$id){
        //         return $letr." ".$id;
        //     })->whereAlpha('letr')->whereNumber('id');
    //}

    // Apartado 1 --- A33
    //{
        // Route::get('/inicio', function () {
        //     return view('home');
        // });
        // Route::get('/fecha/{day?}/{month?}/{year?}',function($day = 25,$month = 10,$year = 1999){
        //     return view('fecha', ['day' => $day, 'month' => $month, 'year' => $year]);
        // });
        // Route::get('/fecha/{day?}/{month?}/{year?}',function($day = 25,$month = 10,$year = 1999){
        //     $fecha = array('day','month','year');
        //     return view('fecha', compact($fecha));
        // });
        // Route::get('/fecha/{day?}/{month?}/{year?}',function($day = null,$month = null,$year = null){
        //     $callback = function ($valor){
        //         return (is_null($valor)) ? " 00 " : $valor;
        //     };
        //     return view('fecha', ['day' => with($day, $callback), 'month' => with($month,$callback), 'year' => with($year,$callback)]);
        // });
        // Route::get('/welcome', function () {
        //     return view('welcome');
        // });
    //}
//}

//Ejercicio A34
//{
    // Route::get('/master', function () {
    //     return view('/layouts/master');
    // });
//}

//Ejercicio A35
//{
    Route::get('/', [HomeController::class, 'home']); 



    Route::get('catalog/index', [CatalogController::class, 'index']);
    Route::get('catalog/show/{id}', [CatalogController::class, 'show']); 
    Route::get('catalog/create', [CatalogController::class, 'create']); 
    Route::get('catalog/edit/{id}', [CatalogController::class, 'edit']); 

    Route::get('catalog/delete/{id}', function ($id) {
        return view('catalog/delete', compact('id'));
    });

    Route::get('auth/login', function () {
        return view('auth/login');
    });

    Route::get('auth/logout', function () {
        return ('Logout usuario');
    });

    Route::resource ('/index', CatalogController::class);
//}

//Ejercicio A36
//{
    // Route::view('/', 'home')->middleware('language');

    // Route::('/request', function(){
    //     $AAA = new treintayseis();
    //     $AAA -> index(treintayseis.re)
    // });
//}

