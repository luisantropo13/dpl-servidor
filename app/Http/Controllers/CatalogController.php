<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    
    public function index(){
        $clientes = Cliente::paginate(4);
        return view('catalog.index',['cliente'=>$clientes]);
    }

    public function create(){
        return view("catalog/create");
    }

    public function show($id){
        $clientes = Cliente::FindOrFail($id);
        return view('catalog.show', array('cliente'=>$clientes));
    }

    public function edit($id){
        $clientes = Cliente::FindOrFail($id);
        return view('catalog.edit', array('cliente'=>$clientes));
    }
}
