<?php

namespace App\Http\Controllers;

use App\Models\Models\PruebaModelo;
use Illuminate\Http\Request;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Models\PruebaModelo  $pruebaModelo
     * @return \Illuminate\Http\Response
     */
    public function show(PruebaModelo $pruebaModelo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Models\PruebaModelo  $pruebaModelo
     * @return \Illuminate\Http\Response
     */
    public function edit(PruebaModelo $pruebaModelo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Models\PruebaModelo  $pruebaModelo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PruebaModelo $pruebaModelo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Models\PruebaModelo  $pruebaModelo
     * @return \Illuminate\Http\Response
     */
    public function destroy(PruebaModelo $pruebaModelo)
    {
        //
    }
}
