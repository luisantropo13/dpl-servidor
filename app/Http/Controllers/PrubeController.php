<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrubeController extends Controller
{
    public function index(){
        return "index";
    }

    public function create(){
        return "create";
    }

    public function show($id){
        return "show $id";
    }

    public function edit($id){
        return "edit $id";
    }
}
