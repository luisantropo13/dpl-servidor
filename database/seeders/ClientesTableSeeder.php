<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use App\Models\Cliente;
use Illuminate\Database\Seeder;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->delete();

        Cliente::create(['name' => 'Jesus', 'fecha_nacimiento' => '2000-01-05', 'correo' => 'arribi@arribi.es']);
        Cliente::create(['name' => 'Pedro', 'fecha_nacimiento' => '2001-01-05', 'correo' => 'pedro@arribi.es']);
        Cliente::factory()->count(50)->create();
    }
}
